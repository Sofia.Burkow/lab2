package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    private final int maxSize = 20;
    List<FridgeItem> items;

    public Fridge() {
        items = new ArrayList<>();
    }

    public Fridge(List<FridgeItem> items) {
        if (items.size() > maxSize) {
            throw new IndexOutOfBoundsException("The fridge cannot store this many items.");
        }
        this.items = items;
    }

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()) {
            items.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!items.contains(item)) {
            throw new NoSuchElementException();
        }
        items.remove(item);
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expired = new ArrayList<>();

        for (FridgeItem item : items) {
            if (item.hasExpired() == true) {
                expired.add(item);
            }
        }

        for (FridgeItem ex : expired) {
            items.remove(ex);
        }
        
        return expired;
    }
}